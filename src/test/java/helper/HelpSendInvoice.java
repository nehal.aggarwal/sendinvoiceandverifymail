package helper;

import java.util.Set;

import org.openqa.selenium.WebDriver;

import pageObjects.DashboardPage;
import pageObjects.LoginPage;
import pageObjects.PortalPage;

public class HelpSendInvoice {
	
	WebDriver driver;
	static String transactionId;
	
	public HelpSendInvoice(WebDriver driver) {
		this.driver=driver;
	}
	
	public void help_sendInvoice() {
		LoginPage lp=new LoginPage(driver);
		lp.clickAdminLogin();
		lp.clickLoginWithHub();
		String login_email="qc2@gmail.com",password="Payu@1234";
		lp.login(login_email,password);
		
		
		PortalPage pp=new PortalPage(driver);
		pp.click_merchantList();
		Set<String> windows=pp.click_viewDashboard();
		int i=1;
		for(String handle:windows) {
			if(i!=1) {
				driver.switchTo().window(handle);
			}
			i++;
		}
		
	    transactionId=GenerateRandomString.randomString(10);
		DashboardPage dp=new DashboardPage(driver);
		dp.click_newEmailInvoice();
		String name="Nehal",
				email="aggarwalnehal55@gmail.com",
				amount="10",
				phone="9896101251",
				description="cjebfuhv";
		dp.fillDetails(name,email,amount,phone,transactionId,description);
		dp.sendInvoice();
		dp.close_newInvoice();
		dp.click_signOut();
	}
	
	public static String get_id() {
		return transactionId;
	}

}
