package test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import config.ConfigFileReader;
import helper.HelpGmailCheck;
import helper.HelpSendInvoice;
import helper.SelectBrowser;


public class TestCases {
	
	static WebDriver driver;
	String URL;
	@BeforeTest
	public void start_browser() {
		SelectBrowser sb=new SelectBrowser();
	    driver=sb.create("chrome");
		driver.manage().window().maximize();
		URL = ConfigFileReader.getConfigValue("url");
		
		
	}
	
	@Test
	public void verify() {
		driver.get(URL);
		HelpSendInvoice h1=new HelpSendInvoice(driver);
		h1.help_sendInvoice();
		
		driver.get("https://gmail.com");
		HelpGmailCheck h2=new HelpGmailCheck(driver);
		h2.help_gmail();

	}
	
	@AfterTest
	public void close_browser() {
		driver.quit();
	}

}
