package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DashboardPage {
	
WebDriver driver;
	
	public DashboardPage(WebDriver driver) {
		this.driver=driver;
	}
	
	public void click_newEmailInvoice() {
		driver.findElement(By.xpath(".//div[@class='buttons']//a[1]")).click();
		
	}

	@SuppressWarnings("deprecation")
	public void fillDetails(String name,String email,String amount,String phone,String transactionId,String description ) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//input[@name='firstname']")));
		
		driver.findElement(By.xpath(".//input[@name='firstname']")).sendKeys(name);
		driver.findElement(By.xpath(".//input[@name='email']")).sendKeys(email);
		driver.findElement(By.xpath(".//input[@name='amount']")).sendKeys(amount);
		driver.findElement(By.xpath(".//input[@name='phone']")).sendKeys(phone);
		driver.findElement(By.xpath(".//input[@name='txnid']")).sendKeys(transactionId);
		driver.findElement(By.xpath(".//textarea[@name='productinfo']")).sendKeys(description);
		driver.findElement(By.xpath(".//button[@class='btn btn-success'][@type='submit']")).click();
		
	}

	@SuppressWarnings("deprecation")
	public void sendInvoice() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//select[@class='txtbox left ng-pristine ng-valid']")));
		
		Select drpdwn=new Select(driver.findElement(By.xpath(".//select[@class='txtbox left ng-pristine ng-valid']")));
		drpdwn.selectByIndex(1);
		driver.findElement(By.xpath(".//a[@class='btn btn-success']")).click();
	}
	
	@SuppressWarnings("deprecation")
	public void close_newInvoice() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='right icon-font']")));
		driver.findElement(By.xpath(".//div[@class='right icon-font']")).click();
	}
	
	@SuppressWarnings("deprecation")
	public void click_signOut() {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='dropdown']/a/span[contains(.,'Account')]")));
		driver.findElement(By.xpath(".//div[@class='dropdown']/a/span[contains(.,'Account')]")).click();
		
		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='dropdown open']/ul/li[5]")));
		driver.findElement(By.xpath(".//div[@class='dropdown open']/ul/li[5]")).click();
	}
	
	
	

}
