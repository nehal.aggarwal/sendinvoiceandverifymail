package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class GmailLogin {
	
	WebDriver driver;
	
	public GmailLogin(WebDriver driver) {
		this.driver=driver;
	}

	public void email(String email) {
		driver.findElement(By.id("identifierId")).sendKeys(email+Keys.RETURN);
		
	}
	
	public void password(String password) {
		 @SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@name='password']")));
		 driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password+Keys.RETURN);
		  
	}
	public void verify_login() {
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver, 50);
		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//a[@class='gb_D gb_Oa gb_i']")));
		
		driver.findElement(By.xpath(".//a[@class='gb_D gb_Oa gb_i']")).click();
		Assert.assertTrue(driver.findElement(By.xpath(".//div[@class='gb_ob gb_pb']")).isDisplayed());
		driver.findElement(By.xpath(".//a[@class='gb_D gb_Oa gb_i']")).click();
	}
	
	

}
