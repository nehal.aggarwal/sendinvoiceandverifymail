package pageObjects;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PortalPage {
	
WebDriver driver;
String portal;
Set<String> windows;
	
	public PortalPage(WebDriver driver) {
		this.driver=driver;
	}
	
	public void click_merchantList() {
		driver.findElement(By.xpath(".//a[@href='https://pp32admin.payu.in/merchantList']")).click();
	}
	
	public Set<String> click_viewDashboard() {
		portal=driver.getWindowHandle();
		//System.out.println(portal);
		driver.findElement(By.xpath(".//a[@href='https://pp32admin.payu.in/viewDashboard?mid=2&aid=32']")).click();
		windows=driver.getWindowHandles();
		//System.out.println(windows);
		//windows.remove(portal);
		return windows;
	}

	

}
