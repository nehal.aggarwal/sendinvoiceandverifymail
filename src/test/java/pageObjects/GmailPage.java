package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class GmailPage {
	
WebDriver driver;
	
	public GmailPage(WebDriver driver){
		this.driver=driver;
		
		}
	
	@SuppressWarnings("deprecation")
	public void search_mail(String search,String id) {
		
//		WebDriverWait wait = new WebDriverWait(driver, 30);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='n6']")));
//		driver.findElement(By.xpath(".//div[@class='n6']")).click();
//		
//		wait = new WebDriverWait(driver, 30);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//a[@title='Spam']")));
//		driver.findElement(By.xpath(".//a[@title='Spam']")).click();
//		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//input[@aria-label]")));
		driver.findElement(By.xpath(".//input[@aria-label='Search mail']")).sendKeys(search+Keys.RETURN);
		
		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//div[@class='ae4 UI']//tbody/tr[contains(.,'"+id+"')]")));
		String path=".//div[@class='ae4 UI']//tbody/tr[contains(.,'"; 
		path+= id+ "')]";
		Assert.assertTrue(driver.findElement(By.xpath(path)).isDisplayed());
		
	}
	

}
