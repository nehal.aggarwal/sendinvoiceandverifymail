package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class LoginPage {
	
WebDriver driver;
	
	public LoginPage(WebDriver driver) {
		this.driver=driver;
	}
	
	public void clickAdminLogin() {
		driver.findElement(By.xpath(".//a[@href='https://pp32admin.payu.in/login']")).click();
	}
	
	public void clickLoginWithHub() {
		driver.findElement(By.xpath(".//a[@href='https://pp32admin.payu.in/hubLogin']")).click();
	}
	
	public void login(String email, String password) {
		driver.findElement(By.xpath(".//input[@id='user_login']")).sendKeys(email);
		driver.findElement(By.xpath(".//input[@id='user_password']")).click();
		driver.findElement(By.xpath(".//input[@id='user_password']")).sendKeys(password+Keys.ENTER);
		Assert.assertTrue(driver.findElement(By.xpath(".//h1")).isDisplayed(),"Login problem");
	}
	
	

}
